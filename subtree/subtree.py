import unittest

def isSubtree(t1, t2):
    if (t2 == None):
        return True
    if (t1 == None):
        return False
    if (t1.val == t2.val):
        return isSubtree(t1.left, t2.left) and isSubtree(t1.right, t2.right)
    elif (isSubtree(t1.left, t2) or isSubtree(t1.right, t2)):
        return True
    return False

class Node:
    def __init__(self, left=None, right=None, val=0):
        self.left = left
        self.right = right
        self.val = val

class TestSubtree(unittest.TestCase):
    def setUp(self):
        pass

    def test_bothempty(self):
        t1 = None
        t2 = None
        self.assertTrue(isSubtree(t1,t2))

    def test_t2empty(self):
        t1 = Node()
        t2 = None
        self.assertTrue(isSubtree(t1,t2))

    def test_t1empty(self):
        t1 = None
        t2 = Node()
        self.assertFalse(isSubtree(t1,t2))

    def test_subtree(self):
        t1
        t2 = Node()
        self.assertFalse(isSubtree(t1,t2))

    def test_notsubtree(self):
        t1 = None
        t2 = Node()
        self.assertFalse(isSubtree(t1,t2))

if __name__ == "__main__":
    unittest.main()

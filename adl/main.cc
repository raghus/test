#include <iostream>

namespace bren
{
    typedef int BrenType;
    class BrenClass
    {
    };

    void bar(BrenClass bc)
    {
        std::cout << "bar()!" << std::endl;
    }

    void foo(BrenType bt)
    {
        std::cout << (int)bt << std::endl;
    }
}


int main(void)
{
    std::cout << "Hello world" << std::endl;
    bren::BrenClass bc;
    bar(bc);
    /*
    bren::BrenType i = 3;
    foo(i);
    foo(2);
    */
}

#include <QtGui>
#include <QtTest/QtTest>

class TestBenchmark: public QObject
{
    Q_OBJECT

private slots:
    void simple();
    void multiple_data();
    void multiple();
};

void TestBenchmark::simple()
{
    QString str1 = QLatin1String("This is a test string");
    QString str2 = QLatin1String("This is a test string");

    QCOMPARE(str1.localeAwareCompare(str2), 0);

    QBENCHMARK {
        str1.localeAwareCompare(str2);
    }
}

void TestBenchmark::multiple_data()
{
    QTest::addColumn<bool>("useLocalCompare");
    QTest::newRow("locale aware compare") << true;
    QTest::newRow("standard compare") << false;
}

void TestBenchmark::multiple()
{
    QFETCH(bool, useLocalCompare);
    QString str1 = QLatin1String("This is a test string");
    QString str2 = QLatin1String("This is a test string");

    bool result;
    if (useLocalCompare) {
        QBENCHMARK {
            str1.localeAwareCompare(str2);
        }
    } else {
        QBENCHMARK {
            result = (str1 == str2);
        }
    }
    QCOMPARE (result, true);
}

QTEST_MAIN(TestBenchmark);
#include "test-benchmark.moc"

#include <glib.h>
#include "bcjson-encode.h"

typedef struct _BCJson BCJson;
static void test_encode_int(void)
{
    BCJson * jsonmsg = bcjson_new ();
    bcjson_dumps_int (jsonmsg, 5);
    gchar * jsonstring = bcjson_free (jsonmsg, FALSE);

    g_assert_cmpstr (jsonstring, ==, "\"5\"");
    g_free (jsonstring);
}

int main (int argc, char *argv[])
{
    gtk_test_init (&argc, &argv);
    g_test_add_func ("/encoding/integers", test_encode_int);
    return g_test_run ();
}

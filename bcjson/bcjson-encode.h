#ifndef __BCJSON_ENCODE_H_
#define __BCJSON_ENCODE_H_

#include <glib.h>

typedef struct _BCJson BCJson;

BCJson * bcjson_new (void);
gchar * bcjson_free (BCJson * obj, gboolean free_segment);

void bcjson_dumps_int (BCJson * obj, int val);

#endif

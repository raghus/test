#include <glib.h>
#include "bcjson-encode.h"

struct _BCJson
{
    GString * json_data;
};

BCJson * bcjson_new ()
{
    BCJson * obj = g_malloc (sizeof (BCJson));
    if (obj) {
        obj->json_data = g_string_new ("");
    }
    return obj;
}

gchar * bcjson_free (BCJson * obj, gboolean free_segment)
{
    if (obj) {
        return g_string_free (obj->json_data, free_segment);
    }
    return NULL;
}

void bcjson_dumps_int (BCJson * obj, int val)
{
    g_return_if_fail (obj);
    g_string_append (obj->json_data, "\"5\"");
}


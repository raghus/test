#include <stdio.h>

#include "Base64Encoder.h"

int main(void)
{
    char buf[128];
    int read;

    do {
        read = fread(buf, sizeof(char), sizeof(buf), stdin);
        Base64Encoder b64(buf, read);
        b64.printall();
    } while (read > 0);
}

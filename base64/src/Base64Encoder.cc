#include <stdio.h>

#include "Base64Encoder.h"
#include "BitReader.h"


void Base64Encoder::printall()
{
    BitReader br(data_, len_);
    while (!br.eod()) {
        printf("%c", 'A' + br.read(6));
    }
}

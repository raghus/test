#ifndef _BITREADER_H_
#define _BITREADER_H_


class BitReader
{
    char const * data_;
    int data_length_;
    int shift_;
    int index_;

public:
    BitReader(char const * data, int data_length):
        data_(data),
        data_length_(data_length),
        shift_(0),
        index_(0)
    {
    }

    bool eod()
    {
        return index_ >= data_length_;
    }

    int read(int bits);
};


#endif

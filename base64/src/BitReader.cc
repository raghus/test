#include <assert.h>

#include "BitReader.h"

int BitReader::read(int bits)
{
        assert(bits <= 8);

        if(shift_ >= 8) {
            index_++;
            shift_ = 0;
            if(index_ >= data_length_) {
                return 0;
            }
        }

        int val;
        int rem = 8 - shift_;
        if(rem >= bits) {
            val = (data_[index_] >> shift_) & (0xff >> (8 - bits));
            shift_+=bits;
        } else {
            val = read(rem) | (read(bits - rem) << rem);
        }

        return val;
}

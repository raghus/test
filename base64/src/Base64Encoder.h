#ifndef _BASE_64_
#define _BASE_64_

class Base64Encoder
{
    char const * data_;
    int len_;

    public:
    Base64Encoder(char const * data, int len):
        data_(data),
        len_(len)
    {
    }

    void printall();
};

#endif
